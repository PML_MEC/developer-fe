/*
 *  Copyright 2020 Huawei Technologies Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const IndustryEn = [
  {
    value: 0,
    label: 'Smart Park'
  }, {
    value: 1,
    label: 'Smart Supermarket'
  }, {
    value: 2,
    label: 'Industrial Manufacturing'
  }, {
    value: 3,
    label: 'Transportation Logistics'
  }, {
    value: 4,
    label: 'Water Conservancy'
  }, {
    value: 5,
    label: 'Game Competition'
  }, {
    value: 6,
    label: 'OpenSource'
  }, {
    value: 7,
    label: 'Other'
  }
]

const TypeEn = [
  {
    value: 0,
    label: 'Video Application'
  }, {
    value: 1,
    label: 'Game'
  }, {
    value: 2,
    label: 'Video Surveillance'
  }, {
    value: 3,
    label: 'Safety'
  }, {
    value: 4,
    label: 'Blockchain'
  }, {
    value: 5,
    label: 'Smart Device'
  }, {
    value: 6,
    label: 'Internet of Things'
  }, {
    value: 7,
    label: 'Big Data'
  }, {
    value: 8,
    label: 'AR/VR'
  }, {
    value: 9,
    label: 'API'
  }, {
    value: 10,
    label: 'SDK'
  }, {
    value: 11,
    label: 'MEP'
  }
]

export {
  IndustryEn,
  TypeEn
}
