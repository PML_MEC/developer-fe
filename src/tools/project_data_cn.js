/*
 *  Copyright 2020 Huawei Technologies Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const Industry = [
  {
    value: 0,
    label: '智慧园区'
  }, {
    value: 1,
    label: '智慧商超'
  }, {
    value: 2,
    label: '工业制造'
  }, {
    value: 3,
    label: '交通物流'
  }, {
    value: 4,
    label: '水利'
  }, {
    value: 5,
    label: '游戏竞技'
  }, {
    value: 6,
    label: '开源'
  }, {
    value: 7,
    label: '其他'
  }
]

const Type = [
  {
    value: 0,
    label: '视频应用'
  }, {
    value: 1,
    label: '游戏'
  }, {
    value: 2,
    label: '视频监控'
  }, {
    value: 3,
    label: '安全'
  }, {
    value: 4,
    label: '区块链'
  }, {
    value: 5,
    label: '智能设备'
  }, {
    value: 6,
    label: '物联网'
  }, {
    value: 7,
    label: '大数据'
  }, {
    value: 8,
    label: 'AR/VR'
  }, {
    value: 9,
    label: 'API'
  }, {
    value: 10,
    label: 'SDK'
  }, {
    value: 11,
    label: 'MEP'
  }
]

const Architecture = [
  {
    value: 0,
    label: 'x86'
  }, {
    value: 1,
    label: 'ARM64'
  }, {
    value: 2,
    label: 'ARM32'
  }, {
    value: 3,
    label: 'GPU'
  }, {
    value: 4,
    label: 'NPU'
  }
]

export {
  Industry,
  Type,
  Architecture
}
