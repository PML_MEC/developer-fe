## API Open source contribution

Whether you should supplement the API usage first, use the sample, or find the problem, You can click on the editing function in the upper left corner to enter Git and edit it directly in Markdown format. This is an open source document editor so that the API quickly complements optimizations and updates.

## IDE Open source contribution

Our IDE plugin provides an Eclipse-based JAVA version plugin. If you find any problems during use, you can give us feedback, It can also be modified directly in the open source community, and we will periodically release new capability roadmaps and fixes. To provide developers with faster and better developer tools. We also welcome everyone to expand to more languages.

## SDK Open source contribution

If you find that the SDK capabilities we provide are not enough to support your app during the development of the app, you can also log in directly: http://git@139.9.0.160:MEP/mec-web.git

## MEP Open source contribution

MEP is the management platform of MEC, which contains many functions that we need to improve together. A good MEP platform can help more MEC eco-developers, and we can optimize MEP together. To adapt to more platforms, higher efficiency
